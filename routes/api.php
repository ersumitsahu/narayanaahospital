<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/send-sms/{name}/{phoneno}/{email}', function ($name, $phoneno, $email) {
    // $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756'); // For US servers
    // $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756', 'https://api.eu.mailgun.net'); // For EU servers

    // Now, compose and send your message.
    // $mg->messages()->send($domain, $params);
    // $mg->messages()->send('peacockindia.in', [
    //     'from'    => '7hills@peacockindia.in',
    //     'to'      => $customer->email,
    //     'subject' => '7hills Inspection App',
    //     'text'    => ' Dear Customer, Here is your quotation for refurbishing your old furniture with 7Hill Furniture Refurbishing company.',
    //     'attachment' => [['filePath' => public_path('storage/reports/' . $quotation->pdf), 'filename' => $quotation->pdf]],
    // ]);

    //sms - format -> http://trans.kapsystem.com/api/v5/index.php?method=sms&message=hello test API submission Bulk submission&to=98996XXXX&sender={{sender_id}}&api_key={{api_key}}&entity_id=32&template_id=43
    // $msg = 'Thank you for visiting Narayanaa Hospital. Your feedback is very important to us, Pls click here https://bit.ly/3cd7Hiv';
    // $curl = curl_init();
    // curl_setopt_array($curl, array(
    //     CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=Thank you for visiting Narayanaa Hospital. Your feedback is very important to us, Pls click here https://bit.ly/3cd7Hiv&to=9724931&sender=NARHSP&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601933162186128357&template_id=1607100000000109090",
    //     CURLOPT_RETURNTRANSFER => true,
    //     CURLOPT_ENCODING => '',
    //     CURLOPT_MAXREDIRS => 10,
    //     CURLOPT_TIMEOUT => 0,
    //     CURLOPT_FOLLOWLOCATION => true,
    //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //     CURLOPT_CUSTOMREQUEST => 'GET',
    //     CURLOPT_HTTPHEADER => array(
    //         'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
    //     ),
    // ));

    // $response = curl_exec($curl);
    // curl_close($curl);
    // return response()->json(["data" => json_decode($response, true)], 200);
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=Thank%2520you%2520for%2520visiting%2520Narayanaa%2520Hospital.%2520Your%2520feedback%2520is%2520very%2520important%2520to%2520us,%2520Pls%2520click%2520here%2520https://bit.ly/3cd7Hiv&to={$phoneno}&sender=NARHSP&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601933162186128357&template_id=1607100000000109090",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    
    return response()->json(["message" => "Message sent successfully", "data" => json_decode($response, true)], 200);
});
